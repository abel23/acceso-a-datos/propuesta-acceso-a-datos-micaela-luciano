Antes de añadir la propuesta de ejemplo me gustaría recordaros algunas cosas:

- Por favor elegid temáticas que os interesen, os gusten, relativas a actividades en las que estéis involucrados, etc. Siempre es mucho más fácil invertir tiempo y trabajo en algo que os interese, que se pueda poner en práctica, y que sea de vuestro gusto, que en el algo que os sea indiferente/no os guste.

- Habéis tenido varios ejemplos no de propuestas pero sí de descripciones de sistemas modelados el año pasado. No quiero copias. Cada uno tenéis unos intereses y unos gustos. Incluso repitiendo tema cada uno tenéis una visión de las cosas, una experiencia y un enfoque distintos. Haced vuestras las propuestas, a corto y largo plazo os ayudará.

- Si tenéis dudas sobre qué incluir, qué no incluir, si la temática es adecuada, si no, si el formato es correcto, si no, preguntadme por favor.

Ejemplo de propuesta:


El trabajo sera realizado mediante dbforge y con dia.

Este trabajo debe realizarse en 2 horas no vais a tener mas tiempo no quiero excusas!!!.


Trabajo fecha de entrega: 19/12/2020
Hora: 11:00